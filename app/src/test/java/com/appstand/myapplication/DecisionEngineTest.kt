package com.appstand.myapplication

import com.appstand.myapplication.model.Decision
import com.appstand.myapplication.model.DecisionEngine
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test

class DecisionEngineTest {

    private val decisionEngine = DecisionEngine()

    @Before
    fun setup() {
        Dispatchers.setMain(Dispatchers.Unconfined)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun notElligibleForLoan_debt() {
        val personalCode = 40L
        val loanAmount = 1
        val loanPeriodInMonths = 60
        runBlocking {
            val decision =
                decisionEngine.checkIfElligibleForLoan(personalCode, loanAmount, loanPeriodInMonths)
            assertEquals(Decision.NotElligible(0), decision)
        }
    }

    @Test
    fun notElligibleForLoan_segm1() {
        val personalCode = 49002010976L
        val loanAmount = 10000
        val loanPeriodInMonths = 60
        runBlocking {
            val decision =
                decisionEngine.checkIfElligibleForLoan(personalCode, loanAmount, loanPeriodInMonths)
            assertEquals(Decision.NotElligible(6000), decision)
        }
    }

    @Test
    fun notElligibleForLoan_segm2() {
        val personalCode = 49002010987L
        val loanAmount = 10000
        val loanPeriodInMonths = 20
        runBlocking {
            val decision =
                decisionEngine.checkIfElligibleForLoan(personalCode, loanAmount, loanPeriodInMonths)
            assertEquals(Decision.NotElligible(6000), decision)
        }
    }

    @Test
    fun notElligibleForLoan_segm3() {
        val personalCode = 49002010998L
        val loanAmount = 10000
        val loanPeriodInMonths = 9
        runBlocking {
            val decision =
                decisionEngine.checkIfElligibleForLoan(personalCode, loanAmount, loanPeriodInMonths)
            assertEquals(Decision.NotElligible(9000), decision)
        }
    }

    @Test
    fun elligibleForLoan_segm1() {
        val personalCode = 49002010976L
        val loanAmount = 1200
        val loanPeriodInMonths = 12
        runBlocking {
            val decision =
                decisionEngine.checkIfElligibleForLoan(personalCode, loanAmount, loanPeriodInMonths)
            assertEquals(Decision.Elligible(1200), decision)
        }
    }

    @Test
    fun elligibleForLoan_segm2() {
        val personalCode = 49002010987L
        val loanAmount = 1200
        val loanPeriodInMonths = 12
        runBlocking {
            val decision =
                decisionEngine.checkIfElligibleForLoan(personalCode, loanAmount, loanPeriodInMonths)
            assertEquals(Decision.Elligible(3600), decision)
        }
    }

    @Test
    fun elligibleForLoan_segm3() {
        val personalCode = 49002010998L
        val loanAmount = 10000
        val loanPeriodInMonths = 10
        runBlocking {
            val decision =
                decisionEngine.checkIfElligibleForLoan(personalCode, loanAmount, loanPeriodInMonths)
            assertEquals(Decision.Elligible(10000), decision)
        }
    }

    @Test
    fun maxLoanAmountForPeriod_3000() {
        val personalCode = 49002010987L
        val loanPeriodInMonths = 10
        runBlocking {
            val maxAmount =
                decisionEngine.getMaxLoanAmountForPeriod(personalCode, loanPeriodInMonths)
            assertEquals(3000, maxAmount)
        }
    }

    @Test
    fun maxAmountOver10kReturn10k() {
        val personalCode = 49002010987L
        val loanPeriodInMonths = 60
        runBlocking {
            val maxAmount =
                decisionEngine.getMaxLoanAmountForPeriod(personalCode, loanPeriodInMonths)
            assertEquals(10000, maxAmount)
        }
    }

    @Test
    fun minPeriodGreaterThan0() {
        val personalCode = 49002010987L
        val amount = 6000
        runBlocking {
            val minPeriod = decisionEngine.getMinPeriod(personalCode, amount)
            assertEquals(20, minPeriod)
        }
    }

    @Test
    fun minPeriod0() {
        val personalCode = 73L
        val amount = 10000
        runBlocking {
            val minPeriod = decisionEngine.getMinPeriod(personalCode, amount)
            assertEquals(0, minPeriod)
        }
    }
}
