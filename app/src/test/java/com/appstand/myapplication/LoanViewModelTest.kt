package com.appstand.myapplication

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.appstand.myapplication.loan.LoanViewModel
import com.appstand.myapplication.model.LoanDecision
import com.appstand.myapplication.model.LoanRepository
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import junit.framework.Assert.assertNull
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class LoanViewModelTest {

    private lateinit var viewModel: LoanViewModel

    private val repository = FakeRepository()

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        Dispatchers.setMain(Dispatchers.Unconfined)
        viewModel = LoanViewModel(repository)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun personalCodeValid_loanPeriodValid_newMaxAmount() {
        // check initial values
        assertEquals(true, viewModel.state.value?.isPersonalCodeValid)
        assertEquals(true, viewModel.state.value?.isLoanPeriodValid)
        assertEquals(true, viewModel.state.value?.isLoanAmountValid)

        // errors
        viewModel.checkIfElligible("msalms", "snjakns", "dmksamdkl")
        assertEquals(false, viewModel.state.value?.isPersonalCodeValid)
        assertEquals(false, viewModel.state.value?.isLoanPeriodValid)
        assertEquals(false, viewModel.state.value?.isLoanAmountValid)
        assertNull(viewModel.state.value?.maxAmount)

        // no more errors
        val maxAmount = 2500
        repository.maxAmount = maxAmount
        viewModel.checkMaxAmount("38101010101", "17", "")
        assertEquals(true, viewModel.state.value?.isPersonalCodeValid)
        assertEquals(true, viewModel.state.value?.isLoanPeriodValid)
        assertEquals(maxAmount, viewModel.state.value?.maxAmount)
    }

    @Test
    fun checkIfElligible_then_showDialog() {
        repository.decision = LoanDecision.Elligible(10000)
        viewModel.checkIfElligible("38101010341", "50", "9000")
        assertEquals(true, viewModel.state.value?.isPersonalCodeValid)
        assertEquals(true, viewModel.state.value?.isLoanPeriodValid)
        assertEquals(true, viewModel.state.value?.isLoanAmountValid)
        assertNotNull(viewModel.state.value?.loanDecision)

        viewModel.dialogShown()
        assertNull(viewModel.state.value?.loanDecision)
    }

    // TODO MORE TESTS IF NEEDED, BUT TAKES TOO LONG :) HOPE ITS OK!
}

class FakeRepository : LoanRepository {

    var decision: LoanDecision? = null
    var maxAmount = 0
    var minPeriod = 0

    override suspend fun checkIfElligibleForLoan(
        personalCode: Long,
        loanPeriodInMonths: Int,
        loanAmount: Int
    ): LoanDecision {
        return decision!!
    }

    override suspend fun getMaxLoanAmountForPersonalcode(
        personalCode: Long,
        loanPeriodInMonths: Int
    ): Int {
        return maxAmount
    }

    override suspend fun getMinPeriodForAmount(personalCode: Long, loanAmount: Int): Int {
        return minPeriod
    }
}
