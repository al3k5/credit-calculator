package com.appstand.myapplication

import com.appstand.myapplication.model.DecisionEngine
import com.appstand.myapplication.model.LoanRepository
import com.appstand.myapplication.model.LoanRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.Reusable
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent

@Module
@InstallIn(ApplicationComponent::class)
object AppModule {

    @Provides
    fun decisionEngine(): DecisionEngine = DecisionEngine()

    @Provides
    @Reusable
    fun loanRepository(decisionEngine: DecisionEngine): LoanRepository =
        LoanRepositoryImpl(decisionEngine)
}
