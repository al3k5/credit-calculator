package com.appstand.myapplication.model

import javax.inject.Inject

interface LoanRepository {

    suspend fun checkIfElligibleForLoan(
        personalCode: Long,
        loanPeriodInMonths: Int,
        loanAmount: Int
    ): LoanDecision

    suspend fun getMaxLoanAmountForPersonalcode(personalCode: Long, loanPeriodInMonths: Int): Int

    suspend fun getMinPeriodForAmount(personalCode: Long, loanAmount: Int): Int
}

class LoanRepositoryImpl @Inject constructor(private val decisionEngine: DecisionEngine) :
    LoanRepository {

    override suspend fun checkIfElligibleForLoan(
        personalCode: Long,
        loanPeriodInMonths: Int,
        loanAmount: Int
    ): LoanDecision {
        return when (val decision =
            decisionEngine.checkIfElligibleForLoan(personalCode, loanAmount, loanPeriodInMonths)) {
            is Decision.Elligible -> LoanDecision.Elligible(decision.maxSum)
            is Decision.NotElligible -> LoanDecision.NotElligible(decision.maxSum)
        }
    }

    override suspend fun getMaxLoanAmountForPersonalcode(
        personalCode: Long,
        loanPeriodInMonths: Int
    ): Int {
        return decisionEngine.getMaxLoanAmountForPeriod(personalCode, loanPeriodInMonths)
    }

    override suspend fun getMinPeriodForAmount(personalCode: Long, loanAmount: Int): Int {
        return decisionEngine.getMinPeriod(personalCode, loanAmount)
    }
}
