package com.appstand.myapplication.model

class DecisionEngine {

    suspend fun checkIfElligibleForLoan(
        peronalCode: Long,
        loanAmount: Int,
        loanPeriodInMonths: Int
    ): Decision {
        val creditScore = calcCreditScore(peronalCode, loanAmount, loanPeriodInMonths)
        val maxAmount = getMaxLoanAmountForPeriod(peronalCode, loanPeriodInMonths)
        return if (creditScore >= 1) {
            Decision.Elligible(maxAmount)
        } else {
            Decision.NotElligible(maxAmount)
        }
    }

    private fun calcCreditScore(
        peronalCode: Long,
        loanAmount: Int,
        loanPeriodInMonths: Int
    ): Double {
        val creditModifier = getCreditModifier(peronalCode)
        return creditModifier / loanAmount.toDouble() * loanPeriodInMonths.toDouble()
    }

    private fun getCreditModifier(personalCode: Long): Int {
        return when (personalCode) {
            49002010976L -> 100
            49002010987L -> 300
            49002010998L -> 1000
            else -> 0
        }
    }

    fun getMinPeriod(personalCode: Long, loanAmount: Int): Int {
        try {
            val minPeriod = loanAmount / getCreditModifier(personalCode)
            return if (minPeriod <= 60) minPeriod else 0
        } catch (e: ArithmeticException) {
            return 0
        }
    }

    suspend fun getMaxLoanAmountForPeriod(personalCode: Long, loanPeriodInMonths: Int): Int {
        val maxAmount = getCreditModifier(personalCode) * loanPeriodInMonths
        return if (maxAmount < 10000) maxAmount else 10000
    }
}

sealed class Decision {

    abstract val maxSum: Int

    data class Elligible(override val maxSum: Int) : Decision()

    data class NotElligible(override val maxSum: Int) : Decision()
}
