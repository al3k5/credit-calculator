package com.appstand.myapplication.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

sealed class LoanDecision : Parcelable {

    abstract val maxSum: Int

    @Parcelize
    data class Elligible(override val maxSum: Int) : LoanDecision()

    @Parcelize
    data class NotElligible(override val maxSum: Int) : LoanDecision()
}
