package com.appstand.myapplication.loan

import com.appstand.myapplication.model.LoanDecision

data class LoanState(
    val maxAmount: Int? = null,
    val minPeriod: Int? = null,
    val loanDecision: LoanDecision? = null,
    val isInAmountRange: Boolean = false,
    val isInPeriodRange: Boolean = false,
    val isPersonalCodeValid: Boolean = true,
    val isLoanPeriodValid: Boolean = true,
    val isLoanAmountValid: Boolean = true
)
