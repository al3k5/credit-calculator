package com.appstand.myapplication.loan

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.appstand.myapplication.model.LoanRepository
import kotlinx.coroutines.launch

class LoanViewModel @ViewModelInject constructor(
    private val repository: LoanRepository
) : ViewModel() {

    private val _state = MutableLiveData<LoanState>()
    val state = _state

    init {
        _state.value = LoanState()
    }

    fun checkMaxAmount(
        personalCodeString: String,
        loanPeriodInMonthsString: String,
        loanAmountString: String
    ) {
        if (isPersonalCodeValid(personalCodeString) &&
            isLoanPeriodInMonthsValid(loanPeriodInMonthsString)
        ) {
            _state.value = _state.value?.copy(isPersonalCodeValid = true, isLoanPeriodValid = true)
            viewModelScope.launch {
                val maxAmount = repository.getMaxLoanAmountForPersonalcode(
                    personalCodeString.toLong(),
                    loanPeriodInMonthsString.toInt()
                )
                if (maxAmount >= 2000) {
                    _state.value = _state.value?.copy(maxAmount = maxAmount)
                    isInAmountRange(loanAmountString)
                } else {
                    _state.value = _state.value?.copy(maxAmount = null)
                }
            }
        } else {
            _state.value = _state.value?.copy(maxAmount = null)
        }
    }

    fun checkMinPeriodForAmount(
        personalCodeString: String,
        loanPeriodInMonthsString: String,
        loanAmountString: String
    ) {
        if (isPersonalCodeValid(personalCodeString) && isLoanAmountValid(loanAmountString)) {
            _state.value = _state.value?.copy(isPersonalCodeValid = true, isLoanAmountValid = true)
            viewModelScope.launch {
                val minPeriod = repository.getMinPeriodForAmount(
                    personalCodeString.toLong(),
                    loanAmountString.toInt()
                )
                if (minPeriod >= 12 && minPeriod <= 60) {
                    _state.value = _state.value?.copy(minPeriod = minPeriod)
                    isInPeriodRange(loanPeriodInMonthsString)
                } else {
                    _state.value = _state.value?.copy(minPeriod = null)
                }
            }
        }
    }

    fun checkIfElligible(
        personalCodeString: String,
        loanPeriodInMonthsString: String,
        loanAmountString: String
    ) {
        val isPersonalCodeValid = isPersonalCodeValid(personalCodeString)
        val isLoanPeriodValid = isLoanPeriodInMonthsValid(loanPeriodInMonthsString)
        val isLoanAmountValid = isLoanAmountValid(loanAmountString)
        _state.value = _state.value?.copy(
            isLoanAmountValid = isLoanAmountValid,
            isLoanPeriodValid = isLoanPeriodValid,
            isPersonalCodeValid = isPersonalCodeValid
        )
        if (isLoanAmountValid && isLoanPeriodValid && isPersonalCodeValid) {
            viewModelScope.launch {
                val decision = repository.checkIfElligibleForLoan(
                    personalCodeString.toLong(),
                    loanPeriodInMonthsString.toInt(),
                    loanAmountString.toInt()
                )
                _state.value = _state.value?.copy(
                    loanDecision = decision
                )
            }
        }
    }

    fun isInAmountRange(loanAmountString: String) {
        state.value?.maxAmount?.let { maxAmount ->
            if (isLoanAmountValid(loanAmountString)) {
                val amount = loanAmountString.toInt()
                _state.value = _state.value?.copy(isInAmountRange = amount <= maxAmount)
            }
        }
    }

    fun isInPeriodRange(loanPeriodInMonthsString: String) {
        state.value?.minPeriod?.let { minPeriod ->
            if (isLoanPeriodInMonthsValid(loanPeriodInMonthsString)) {
                val period = loanPeriodInMonthsString.toInt()
                _state.value = _state.value?.copy(isInPeriodRange = period >= minPeriod)
            }
        }
    }

    fun dialogShown() {
        _state.value = _state.value?.copy(loanDecision = null)
    }

    private fun isPersonalCodeValid(personalCode: String): Boolean {
        return personalCode.length > 10 && personalCode.toLongOrNull() != null
    }

    private fun isLoanPeriodInMonthsValid(loanPeriodInMonths: String): Boolean {
        val loanPeriod = loanPeriodInMonths.toIntOrNull()
        return loanPeriod != null && loanPeriod <= 60 && loanPeriod >= 12
    }

    private fun isLoanAmountValid(loanAmount: String): Boolean {
        val loanAmount = loanAmount.toIntOrNull()
        return loanAmount != null && loanAmount >= 2000 && loanAmount <= 10000
    }
}
