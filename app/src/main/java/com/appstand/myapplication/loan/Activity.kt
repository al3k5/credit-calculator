package com.appstand.myapplication.loan

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isInvisible
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import com.appstand.myapplication.AlertDialogFragment
import com.appstand.myapplication.R
import com.appstand.myapplication.model.LoanDecision
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity.*

@AndroidEntryPoint
class Activity : AppCompatActivity(R.layout.activity) {

    private val viewModel: LoanViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.state.observe(this, Observer { state ->
            personalCodeInput.error = if (!state.isPersonalCodeValid) {
                getString(R.string.loan_personal_code_error)
            } else {
                null
            }
            periodInput.error = if (!state.isLoanPeriodValid) {
                getString(R.string.loan_period_error)
            } else {
                null
            }
            amountInput.error = if (!state.isLoanAmountValid) {
                getString(R.string.loan_amount_error)
            } else {
                null
            }
            state.maxAmount?.let { maxAmount ->
                maxAmountView.text = if (maxAmount > 0) {
                    getString(R.string.loan_max_amount, maxAmount)
                } else {
                    getString(R.string.loan_debt)
                }
            }

            state.minPeriod?.let { minAmount ->
                minPeriodView.text = getString(R.string.loan_period_correction, minAmount)
            }

            maxAmountView.setTextColor(
                ResourcesCompat.getColor(
                    resources,
                    if (state.isInAmountRange) {
                        R.color.colorAccent
                    } else {
                        R.color.colorError
                    }, null
                )
            )
            minPeriodView.setTextColor(
                ResourcesCompat.getColor(
                    resources, if (state.isInPeriodRange) {
                        R.color.colorAccent
                    } else {
                        R.color.colorError
                    }, null
                )
            )
            maxAmountView.isInvisible = state.maxAmount == null
            minPeriodView.isInvisible = state.minPeriod == null
            state.loanDecision?.let { decision ->
                showDialog(decision)
                viewModel.dialogShown()
            }
        })

        personalCodeInput.addTextChangedListener { text ->
            viewModel.checkMaxAmount(
                text.toString(),
                periodInput.text.toString(),
                amountInput.text.toString()
            )
            viewModel.checkMinPeriodForAmount(
                text.toString(),
                periodInput.text.toString(),
                amountInput.text.toString()
            )
        }

        periodInput.addTextChangedListener { text ->
            viewModel.checkMaxAmount(
                personalCodeInput.text.toString(),
                text.toString(),
                amountInput.text.toString()
            )
            viewModel.isInPeriodRange(text.toString())
        }

        amountInput.addTextChangedListener { text ->
            viewModel.checkMinPeriodForAmount(
                personalCodeInput.text.toString(),
                periodInput.text.toString(),
                text.toString()
            )
            viewModel.isInAmountRange(text.toString())
        }

        submitButton.setOnClickListener {
            viewModel.checkIfElligible(
                personalCodeInput.text.toString(),
                periodInput.text.toString(),
                amountInput.text.toString()
            )
        }
    }

    fun showDialog(decision: LoanDecision) {
        val ft: FragmentTransaction = supportFragmentManager.beginTransaction()
        val prev: Fragment? = supportFragmentManager.findFragmentByTag("dialog")
        if (prev != null) {
            ft.remove(prev)
        }
        ft.addToBackStack(null)

        val newFragment: AlertDialogFragment = createAlertDialog(decision)
        newFragment.show(ft, "dialog")
    }
}

fun createAlertDialog(decision: LoanDecision): AlertDialogFragment {
    val f = AlertDialogFragment()
    val args = Bundle()
    args.putParcelable(AlertDialogFragment.parcelTag, decision)
    f.setArguments(args)
    return f
}
