package com.appstand.myapplication

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import com.appstand.myapplication.model.LoanDecision
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class AlertDialogFragment : DialogFragment() {

    companion object {
        val parcelTag = "decision"
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val decision: LoanDecision = requireArguments().getParcelable(parcelTag)!!
        return MaterialAlertDialogBuilder(requireContext())
            .setTitle(R.string.loan_dialog_title)
            .setMessage(
                when (decision) {
                    is LoanDecision.Elligible -> getString(R.string.loan_elligible, decision.maxSum)
                    is LoanDecision.NotElligible -> getString(
                        R.string.loan_not_elligible,
                        decision.maxSum
                    )
                }
            )
            .setPositiveButton(
                getString(android.R.string.ok),
                object : DialogInterface.OnClickListener {
                    override fun onClick(p0: DialogInterface?, p1: Int) {
                        dismiss()
                    }
                })
            .create()
    }
}
