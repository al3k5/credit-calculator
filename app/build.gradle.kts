import com.appstand.myapplication.GradleVersions as Ver

plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("android.extensions")
    kotlin("kapt")
    id("dagger.hilt.android.plugin")
    id("org.jlleitschuh.gradle.ktlint") version "9.2.1"
}

android {
    compileSdkVersion(Ver.Build.compileSdkVersion)
    buildToolsVersion = Ver.Build.buildToolsVersion

    defaultConfig {
        applicationId = "com.appstand.myapplication"
        minSdkVersion(Ver.Build.minSdkVersion)
        targetSdkVersion(Ver.Build.targetSdkVersion)
        versionCode = Ver.Build.versionCode
        versionName = Ver.Build.versionName

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    implementation(Libs.Kotlin.stdlib)

    implementation(Libs.AndroidX.core)
    implementation(Libs.AndroidX.appCompat)
    implementation(Libs.AndroidX.constraintLayout)
    implementation(Libs.AndroidX.hiltLifeCycleViewModel)
    kapt(Libs.AndroidX.hiltAndroidXCompiler)

    implementation(Libs.AndroidX.lifecycleViewModel)
    implementation(Libs.AndroidX.lifecycleLiveData)
    implementation(Libs.AndroidX.lifecycleCommon)
    implementation(Libs.AndroidX.lifecycleExtensions)
    implementation(Libs.AndroidX.activityKtx)

    implementation(Libs.Google.material)

    implementation(Libs.Google.hilt)
    kapt(Libs.Google.hiltAnnotationCompiler)

    testImplementation(Libs.Test.junit)
    testImplementation(Libs.Test.archCoreTesting)
    testImplementation(Libs.Kotlin.coroutinesTest)

    androidTestImplementation(Libs.AndroidX.Test.androidXJunit)
    androidTestImplementation(Libs.AndroidX.Test.espressoCore)
}
