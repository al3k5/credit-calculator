object Libs {
    const val androidGradlePlugin = "com.android.tools.build:gradle:4.0.1"

    object AndroidX {
        private const val hiltAndroidXVersion = "1.0.0-alpha02"
        private const val lifeCycleVersion = "2.2.0"

        const val appCompat = "androidx.appcompat:appcompat:1.2.0"
        const val core = "androidx.core:core-ktx:1.3.1"
        const val activityKtx = "androidx.activity:activity-ktx:1.1.0"

        const val constraintLayout = "androidx.constraintlayout:constraintlayout:2.0.1"
        const val hiltLifeCycleViewModel =
            "androidx.hilt:hilt-lifecycle-viewmodel:$hiltAndroidXVersion"
        const val hiltAndroidXCompiler = "androidx.hilt:hilt-compiler:$hiltAndroidXVersion"

        const val lifecycleViewModel =
            "androidx.lifecycle:lifecycle-viewmodel-ktx:$lifeCycleVersion"
        const val lifecycleLiveData = "androidx.lifecycle:lifecycle-livedata-ktx:$lifeCycleVersion"
        const val lifecycleCommon = "androidx.lifecycle:lifecycle-common-java8:$lifeCycleVersion"
        const val lifecycleExtensions = "androidx.lifecycle:lifecycle-extensions:$lifeCycleVersion"

        object Test {
            const val androidXJunit = "androidx.test.ext:junit:1.1.2"
            const val espressoCore = "androidx.test.espresso:espresso-core:3.3.0"
        }
    }

    object Kotlin {
        private const val version = "1.3.72"
        const val stdlib = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:$version"
        const val kotlinPlugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:$version"
        const val coroutinesTest = "org.jetbrains.kotlinx:kotlinx-coroutines-test:1.3.7"
    }

    object Test {
        const val junit = "junit:junit:4.12"
        const val archCoreTesting = "androidx.arch.core:core-testing:2.1.0"
    }

    object Google {

        private const val hiltVersion = "2.28-alpha"
        const val material = "com.google.android.material:material:1.2.1"
        const val hilt = "com.google.dagger:hilt-android:$hiltVersion"
        const val hiltAnnotationCompiler = "com.google.dagger:hilt-android-compiler:$hiltVersion"
        const val hiltGradlePlugin = "com.google.dagger:hilt-android-gradle-plugin:$hiltVersion"
    }
}
