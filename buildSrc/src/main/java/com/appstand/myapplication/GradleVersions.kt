package com.appstand.myapplication

object GradleVersions {
    object Build {
        const val compileSdkVersion = 29
        const val buildToolsVersion = "29.0.3"
        const val minSdkVersion = 21
        const val targetSdkVersion = 29
        const val versionCode = 1
        const val versionName = "1.0"
    }
}
